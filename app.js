/* global io */
/* global angular */

(function () {
    'use strict';
  angular.module("stackInsight", [
    "ngRoute",
    "toaster",
    "ui.bootstrap",
    "gridster",
    "angular-timezone-select",
    "angular-loading-bar",
    "btford.socket-io",
    "stack.placeHolder",
    "stack.notifier",
    "stack.StackService",
    "stack.AuthService",
    "stack.socket",
    "stack.signup",
    "stack.header",
    "stack.login",
    "stack.dashboard",
    "stack.credentials",
    "stack.userSettings",
    "stack.schedules",
    "stack.actions",
    "stack.donutChart"
  ])
  .factory("AuthInterceptor", function ($q, $location, $window) {
      var interceptorFactory = {};
  		
      interceptorFactory.request = function(config) {
  			config.headers = config.headers || {};
        var user =  JSON.parse($window.sessionStorage.getItem('user'));
  			if (user){ 
          config.headers.Authorization = 'Bearer ' + user.token;
  			}
  			return config;
  		};
  		
  		interceptorFactory.responseError = function(response) {
  			if (response.status === 403 || response.status === 401) {
  				$location.path('/login');
  			}
  			return $q.reject(response);
  		};
  		return interceptorFactory;
    })
    // .constant('APP_URL', 'http://api.actstack.com/v1')
    // .constant('APP_SOCKET_URL', 'http://api.actstack.com')
    
     .constant('APP_URL', 'http://localhost:8080/v1')
     .constant('APP_SOCKET_URL', 'http://localhost:8080')
 
    .config(['$routeProvider', '$httpProvider', '$locationProvider', function ($routeProvider, $httpProvider, $locationProvider) {
      $routeProvider.otherwise({redirectTo: '/login'});
      $httpProvider.interceptors.push('AuthInterceptor');
      //$locationProvider.html5Mode(true);
    }]) 
    .run(['$rootScope', '$location', function ($rootScope, $location) {
      
      $rootScope.$on('$routeChangeSuccess', function (event, next, current) {
        $rootScope.$broadcast('stack.routeChangeEvent', {next: next, current: current});      
      });
      
      $rootScope.$on("$routeChangeError", function(event, current, next, rejection) {
        $location.path('/login');
      });
      
    }]);
})();

