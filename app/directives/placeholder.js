/* global $ */
(function () {
  'use strict';
  angular.module("stack.placeHolder", [])
    .directive('placeholder', function(){
      return {
        restrict: 'A',
        link: function(scope, element, attrs) {
          element.data('holder', attrs.placeholder);
        	element.bind('blur', function(){
        		if(element.val() === ''){
        			$(element).attr('placeholder', element.data('holder'));
        		}
        	});
          element.bind('focus', function(){
        	  $(element).attr('placeholder', '');
          });
        }
      }
	});
})();
