    angular.module("stack.notifier", []).factory("Notifier", function ($rootScope, $location, toaster) {
        /**
        * Constructor
        * @argument {String} title 
        */
        function Notifier(title) {
            this.title = title;
            this.timeout = 5000;
            var _notifications = [];
            /**
             * Private functions
             */
            function datetime() {
                var d = new Date();
                var minutes = d.getMinutes().toString().length === 1 ? '0' + d.getMinutes() : d.getMinutes();
                var hours = d.getHours().toString().length === 1 ? '0' + d.getHours() : d.getHours();
                var ampm = d.getHours() >= 12 ? 'pm' : 'am';
                var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
                var days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
                return days[d.getDay()] + ' ' + months[d.getMonth()] + ' ' + d.getDate() + ' ' +
                        d.getFullYear() + ' ' + hours + ':' + minutes + ampm;
            }
    
            function notifyWithDateTime(notification) {
                return notification + '</br>' + datetime();
            }
    
            /**
             * Public method, assigned to prototype
             */
            return {
                // Getter/Setter
                notifications: function (value) {
                    if (!arguments.length) {
                        return _notifications;
                    }
                    _notifications.push(value);
                    $rootScope.$broadcast('notifier:add', true);
                },
                onSuccess: function (notification) {
                    if (!notification) {
                        return;
                    }
                    var notifyObj = {type: 'success', title: this.title, body: notifyWithDateTime(notification)};
                    this.notifications(notifyObj);
                    return toaster.pop(notifyObj.type, notifyObj.title, notifyObj.body, this.timeout,"trustedHtml");
                },
                onError: function (notification) {
                    if (!notification) {
                        return;
                    }
                    var notifyObj = {type: 'error', title: this.title, body: notifyWithDateTime(notification)};
                    this.notifications(notifyObj);
                    return toaster.pop(notifyObj.type, notifyObj.title, notifyObj.body, this.timeout, "trustedHtml");
                },
                showNotifications: function () {
                    var notifications = this.notifications();
                    var count = 0;
                    angular.forEach(notifications, function (notify, index) {
                        if(index < 5){
                           return toaster.pop(notify.type, notify.title, notify.body, this.timeout, "trustedHtml");
                        }    
                    });
                }
    
            };
        }
    
    
        /**
         * Static method, assigned to class
         * @argument {String} title description
         */
        Notifier.build = function (title) {
            return new Notifier(title);
        };
    
        /**
         * Return the constructor function
         */
        return Notifier;
    
    });
