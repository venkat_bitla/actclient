(function () {
    'use strict';
    var stackSocket = function (socketFactory, APP_SOCKET_URL, $window) {
        var iosocket = io.connect(APP_SOCKET_URL);
        var stackSocket = socketFactory({
            ioSocket: iosocket
        });
        return stackSocket;
    }

    angular.module("stack.socket", [])
        .factory("stackSocket", stackSocket);

})();

