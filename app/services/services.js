
(function () {

    'use strict';

    var StackService = function ($http, $q, $window, APP_URL) {
         
        var Stack = {};
  
 		//User
		Stack.signup = function(user) {
            return $http.post(APP_URL + '/user', user);            
        };
   
        Stack.login = function(email, password) {
        	return $http.post(APP_URL + '/login', { email: email, password: password});
        };
        
        Stack.getUser = function() {
       		return $http.get(APP_URL + '/user', { cache: true });
        };
        
        Stack.currentUser = function(){
            return  JSON.parse($window.sessionStorage.getItem('user'));
        };

        Stack.passwordToken = function(email){
            return $http.post(APP_URL + '/forgotPassword', email);
        };

        Stack.resetPassword = function(userToken, resetPasswordObj){
            return $http.post(APP_URL + '/forgotPassword/' + userToken, resetPasswordObj);
        };
   
   		//Credential
        Stack.createCredential = function(credential) {
            return $http.post(APP_URL + '/credentials', credential);
        };

        Stack.verifyEmail = function(userToken){
            return $http.get(APP_URL + '/verifyEmail/' + userToken);
        };

        Stack.updateCredential = function(credential) {
            var credentialId = credential.id
            var updateCredential = {
                name: credential.name,
                type: credential.type,
                iamRole: credential.iamRole,
                password: credential.password,
                accessKey: credential.accessKey,
                secretKey:credential.secretKey,
                region:credential.region
            }
            return $http.put(APP_URL + '/credentials/' + credentialId, updateCredential);
        };
        

        Stack.getCredential = function(credentialId) {
            return $http.get(APP_URL + '/credentials/' + credentialId);
        };        

        Stack.getUserCredentials = function(userId) {
            return $http.get(APP_URL + '/' + userId + '/credentials');
        };        
        
        Stack.deleteCredential = function(credentialId) {
            return $http.delete(APP_URL + '/credentials/' + credentialId);
        };
        
        //Dashboard Summary
        Stack.getAwsSummary = function(userId) {
            return $http.get(APP_URL + '/' + userId + '/summary');   
        };
        
        //Schedule
        Stack.createSchedule = function(schedule) {
            return $http.post(APP_URL + '/schedules', schedule);
        };

        Stack.updateSchedule = function(scheduleId, schedule) {
            return $http.put(APP_URL + '/schedules/' + scheduleId, schedule);
        };

        Stack.getSchedule = function(scheduleId) {
            return $http.get(APP_URL + '/schedules/' + scheduleId);
        };       

        Stack.getUserSchedules = function(userId) {
            return $http.get(APP_URL + '/' + userId + '/schedules');
        };        
        
        Stack.deleteSchedule = function(scheduleId) {
            return $http.delete(APP_URL + '/schedules/' + scheduleId);
        };

        //Actions
        Stack.createAction = function(action) {
            return $http.post(APP_URL + '/actions', action);
        };

        Stack.updateAction = function(actionId, action) {
            return $http.put(APP_URL + '/actions/' + actionId, action);
        };

        Stack.getAction = function(actionId) {
            return $http.get(APP_URL + '/actions/' + actionId);
        };       

        Stack.getUserActions = function(userId) {
            return $http.get(APP_URL + '/' + userId + '/actions');
        };        
        
        Stack.deleteAction = function(actionId) {
            return $http.delete(APP_URL + '/actions/' + actionId);
        };
        
        //Instances
        Stack.getUserInstances = function(userId) {
            return $http.get(APP_URL + '/' + userId + '/instances');
        };    

        return Stack;
    };

    angular.module("stack.StackService", [])
        .factory("StackService", StackService);

})();
