(function () {
    
    'use strict'; 

    var AuthService = function ($rootScope, $location, $q, StackService) {
        var AuthService = {}
        var init =  function() {
            if(StackService.currentUser()){
                $location.path('/dashboard');   
             }
        }
        
        AuthService.loginRequired = function() {
            var deferred = $q.defer();
            if (!StackService.currentUser()) {
                deferred.reject();
            } else {
                deferred.resolve();
            }
            return deferred.promise;
        }

        AuthService.hasUserLoggedIn = function () {
            var defer = $q.defer();
            if(StackService.currentUser()){
                defer.reject();
            } else {
                defer.resolve();
            }
            return defer.promise;
        }
        
        init();
        return AuthService;
    }

	angular.module("stack.AuthService", [])
		.factory("AuthService", AuthService);
})();
