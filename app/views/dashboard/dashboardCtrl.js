/* global d3 */
(function () {
    
    function buildDonutChart(dataset){
        var chartWidth = 240;
        var chartHeight = 150;
        var _outerRadius = 100;
        var _innerRadius = 60;
        
        var maxEleastic_ips = dataset.eleastic_ips.max;
        var usedEleastic_ips = dataset.eleastic_ips.used;
        var maxEc2_instances =  dataset.ec2_instances.max;
        var usedEc2_instances =  dataset.ec2_instances.used;
        
        var elasticScale = d3.scale.linear()
            .domain([0, maxEleastic_ips])
            .range([-90, 90]);
        
        var ec2Scale = d3.scale.linear()
            .domain([0, maxEc2_instances])
            .range([-90, 90]);
        
        var elasticSvg = d3.select('#eleastic_ips')
            .append('svg')
            .attr('width', chartWidth)
            .attr('height', chartHeight);

        var ec2Svg = d3.select('#ec2_instances')
            .append('svg')
            .attr('width', chartWidth)
            .attr('height', chartHeight);
        
        var maxarc =  d3.svg.arc()
            .outerRadius(_outerRadius)
            .innerRadius(_innerRadius)
            .startAngle(-Math.PI/2)
            .endAngle(Math.PI/2);
        
        var eleasticUsageArc = d3.svg.arc()
            .outerRadius(_outerRadius)
            .innerRadius(_innerRadius)
            .startAngle(function(){
            return (-Math.PI/2);
        })
        .endAngle(function(){
            return elasticScale(usedEleastic_ips) * (Math.PI/180);
        });
        
        var ec2UsageArc = d3.svg.arc()
            .outerRadius(_outerRadius)
            .innerRadius(_innerRadius)
            .startAngle(function(){
                return (-Math.PI/2);
            })
            .endAngle(function(){
                return ec2Scale(usedEc2_instances) * (Math.PI/180);
            });
        
        // elasticSvg
        // TODO: write a common method to build the donuts.
        elasticSvg.append("g")
            .append("path")
            .attr("d", maxarc)
            .attr("fill", function () {
                return '#71c84c';
            })
            .attr("transform", "translate(130, 145)");
            elasticSvg.append("g")
                .append("path")
                .attr("d", eleasticUsageArc)
                .attr("fill", function() {
                    return '#FF8106';
                })
                .attr("transform", "translate(130, 145)");
            elasticSvg.append("svg:text")
                .attr("text-anchor", "middle")
                .attr("font-size", "40px")
                .attr("font-weight", 'bold')
                .attr("fill", "#605e5f")
                .attr("transform", "translate(130, 145)")
                .text(usedEleastic_ips);
             
             if (usedEleastic_ips > 0) {
                elasticSvg.append("svg:text")
                .attr("text-anchor", "middle")
                .attr("font-size", "12px")
                .attr("fill", "#FFFFFF")
                .attr("transform", "translate(50, 135)")
                .text('Used');
             }
             
             ec2Svg.append("g")
                .append("path")
                .attr("d", maxarc)
                .attr("fill", function() {
                    return '#71c84c';
                })
                .attr("transform", "translate(130, 145)");
                
             ec2Svg.append("g")
                .append("path")
                .attr("d", ec2UsageArc)
                .attr("fill", function() {
                    return '#FF8106';
                })
                .attr("transform", "translate(130, 145)");
             
             ec2Svg.append("svg:text")
                .attr("text-anchor", "middle")
                .attr("font-size", "40px")
                .attr("font-weight", 'bold')
                .attr("fill", "#605e5f")
                .attr("transform", "translate(130, 145)")
                .text(usedEc2_instances);
                if(usedEc2_instances > 0){
                    ec2Svg.append("svg:text")
                        .attr("text-anchor", "middle")
                        .attr("font-size", "12px")
                        .attr("fill", "#FFFFFF")
                        .attr("transform", "translate(50, 135)")
                        .text('Used');
                }
     };
    
    //dashboardController
    function dashboardController($scope, $rootScope, $location, $window,
        StackService, stackSocket, Notifier) {

        var notifier = Notifier.build('Dashboard');
        $scope.showNotifications = function(){
            notifier.showNotifications();
        };

        $scope.gridsterOpts = {
            resizable: { enabled: false},
            draggable: { enabled: false}
        }



        $scope.summary = {};
        $scope.donutCharts = ['used_elastic_ips']
        $scope.userId = StackService.currentUser().id;
        StackService.getAwsSummary($scope.userId).success(function (response) {
            if (response) {
                $scope.summary = JSON.parse(response.data);
                setTimeout(function () {
                    if($scope.summary){
                        buildDonutChart($scope.summary);
                    }
                }, 1000);
            }
        }).error(function (data, status) {
            notifier.onError(data.message);
        });

        $scope.stripUnderscore = function(keyStr){
            if(keyStr){
                return keyStr.replace('_', " ");
            }
        };

        $scope.resourcesUsed = function(keyVal){
            return keyVal.used;
        };

        $scope.testSummaryObj = function(keyVal){
            return (typeof keyVal);
        };

        $scope.resourcesLeft = function(keyVal){
            return (keyVal.max - keyVal.used);
        };

        stackSocket.on('connect', function () {
            var authToken = JSON.parse($window.sessionStorage.getItem('user'));
            stackSocket.emit('authenticate', {token: authToken.token});
        });

        stackSocket.on('summary', function (data) {
           $scope.$apply(function () {
            $scope.summary =  JSON.parse(data.data);
           });
        });

       stackSocket.on('news', function (data) {
        console.log(data);
       });
    }

    function config($routeProvider) {
        $routeProvider.when("/dashboard", {
            templateUrl: "/app/views/dashboard/dashboard.html",
            controller: "dashboardController",
            resolve: {
                loginRequired: function (AuthService) {
                    return AuthService.loginRequired();
                }
            }
        });
    }

    dashboardController.$inject =  ['$scope', '$rootScope', '$location', '$window',
        'StackService', 'stackSocket', 'Notifier'];

    angular.module("stack.dashboard", ["ngRoute"])
        .config(["$routeProvider", config])
        .controller("dashboardController", dashboardController);
})();



