"use strict";
(function(){
    
    //userSettingsController
    function userSettingsController($scope, $rootScope, $location){
        
        $scope.setTimzone = function(){
            console.log($scope.timezone);
        }
        
        $scope.changePassword = function () {
            var oldpass     = $scope.changePassword.password;
            var passwordOne = $scope.changePassword.newPasswordOne;
            var passwordTwo = $scope.changePassword.newPasswordTwo;
            
            if (!angular.equals(passwordOne, passwordTwo)) {
                $scope.changePasswordForm.newPasswordTwo.$dirty = true;
                $scope.changePasswordForm.newPasswordTwo.$invalid = true;
                $scope.changePasswordForm.$invalid = true;
            }
        };
    }

    function config($routeProvider){
        $routeProvider
        .when("/settings", {
            templateUrl: "app/views/userSettings/userSettings.html",
            controller: "userSettingsController",
            resolve: {
                loginRequired: function (AuthService) {
                    return AuthService.loginRequired();
                }
            }
        })

    }

    userSettingsController.$inject =  ["$scope", "$rootScope", "$location"];
    angular.module("stack.userSettings", ["ngRoute"])
        .config(["$routeProvider", config])
        .controller("userSettingsController", userSettingsController)

})();

