/* global angular */
'use strict';
(function () {
    //LoginCtrl
    function LoginCtrl($scope, $rootScope, $location, $modal, $http, $window, StackService, Notifier) {

        var notifier = Notifier.build('Reset Password');
        $scope.showNotifications = function(){
            notifier.showNotifications();
        };


        if($window.sessionStorage.getItem("isLogout")){
            $scope.logoutMessage = true;
            $window.sessionStorage.removeItem("isLogout");
        }
        
        function handleError(response) {
            delete $window.sessionStorage.user;
            console.log('Error: ' + response.data);
            $scope.wrongCredentials = true;
        }
        
        function login(email, password) {
            StackService.login(email, password).success(function(response) {
                if(response){
                    $window.sessionStorage.setItem('user', JSON.stringify(response));
                    $location.path('/dashboard');
                }
            }).error(function (data, status) {
                notifier.onError(data.message);
            });
        }

        $scope.login = function(credentials) {
            if($scope.loginForm.$valid){
                login(credentials.email, credentials.password)
            }
        };

        $scope.resetPassword = function () {
            var modalInstance = $modal.open({
                templateUrl: '/app/views/login/resetPasswordModel.html',
                scope: $scope,
                controller : 'sendResetEmailCtrl'
            });
            modalInstance.result.then(function(emailId){
                StackService.passwordToken({'email': emailId}).success(function(response) {
                    notifier.onSuccess(response.data.message);
                }).error(function (data, status) {
                    notifier.onError(data.message);
                });
            });
        };
    }


    function sendResetEmailCtrl($scope, $modalInstance, StackService) {
        $scope.close = function () {
            $modalInstance.dismiss();
        };
        $scope.sendMail = function () {
            if ($scope.resetPasswordForm.$valid) {
                $modalInstance.close($scope.email);
            }
        };
    }

    function updatePasswordCtrl($scope, $rootScope, $location, $http, $window, StackService, Notifier){
        var notifier = Notifier.build('Update Password');
        $scope.showNotifications = function(){
            notifier.showNotifications();
        };
        
        $scope.updatePassword = function () {
            var userToken = $location.path().split("/")[2] || "Unknown";
            var userNewPassword = $scope.user.password;
            var resetObj = {
                password: userNewPassword
            };
            
            StackService.resetPassword(userToken, resetObj).success(function(response) {
                $location.path('/login');
            }).error(function (data, status) {
                notifier.onError(data.message);
            });
        }
    }

    function config($routeProvider) {
        $routeProvider
            .when('/login', {
                templateUrl: '/app/views/login/login.html',
                controller: 'LoginCtrl',
                resolve: {
                    hasUserLoggedIn: function (AuthService) {
                        return AuthService.hasUserLoggedIn();
                    }
                }
            })
            .when("/forgotEmail/:id", {
                templateUrl: "app/views/login/updatepassword.html",
                controller: "updatePasswordCtrl",
                resolve: {
                    hasUserLoggedIn: function (AuthService) {
                        return AuthService.hasUserLoggedIn();
                    }
                }
            });
    }

    LoginCtrl.$inject = ["$scope", "$rootScope", "$location", "$modal", "$http", "$window", "StackService", "Notifier"];
    sendResetEmailCtrl.$inject = ["$scope", "$modalInstance", "StackService"];
    updatePasswordCtrl.$inject = ["$scope", "$rootScope", "$location", "$http", "$window", "StackService", "Notifier"];
    angular.module('stack.login', ['ngRoute'])
        .controller('LoginCtrl', LoginCtrl)
        .controller('sendResetEmailCtrl', sendResetEmailCtrl)
        .controller('updatePasswordCtrl', updatePasswordCtrl)
        .config(['$routeProvider', config]);

})();

