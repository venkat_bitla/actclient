'use strict';
(function () {
    //SignupCtrl
    function SignupCtrl($scope, $rootScope, $location, $http, StackService, Notifier) {
        
        var notifier = Notifier.build('Signup');
        $scope.showNotifications = function(){
            notifier.showNotifications();  
        };
        
        $scope.singup = function(user) {
       		if($scope.signupForm.$valid){
                StackService.signup(user).success(function(response) {
                    $location.path('/login');
                }).error(function (data, status) {
                    notifier.onError(data.message);
                });
            }
        };

    }

    function verifyEmailCtrl($scope, $rootScope, $location, $http, StackService, Notifier){
        var notifier = Notifier.build('Verify Email');
        $scope.showNotifications = function(){
            notifier.showNotifications();  
        };
        
        var userToken = $location.path().split("/")[2] || "Unknown";
        StackService.verifyEmail(userToken).success(function(response) {
            $location.path('/login');
        }).error(function (data, status) {
            notifier.onError(data.message);
        });
    }

    function config($routeProvider) {
        $routeProvider
            .when('/signup', {
                templateUrl: '/app/views/signup/signup.html',
                controller: 'SignupCtrl',
                resolve: {
                    hasUserLoggedIn: function (AuthService) {
                        return AuthService.hasUserLoggedIn();
                    }
                }
            })
            .when("/verifyEmail/:id", {
                templateUrl: "/app/views/login/login.html",
                controller: "verifyEmailCtrl",
                resolve: {
                    hasUserLoggedIn: function (AuthService) {
                        return AuthService.hasUserLoggedIn();
                    }
                }
            });
    }

    angular.module('stack.signup', ['ngRoute'])
        .controller('SignupCtrl', SignupCtrl)
        .controller('verifyEmailCtrl', verifyEmailCtrl)
        .config(['$routeProvider', config]);

})();

