/* global angular */
"use strict";
(function(){
  
    function range(min, max, step){
        step = step || 1;
        var input = [];
        for (var i = min; i <= max; i += step) {
            input.push(i);
        }
        return input;
    }

    function rangeObj(min, max, step){
        step = step || 1;
        var input = [];
        for (var i = min; i <= max; i += step) {
            input.push({name: i , value: i});
        }
        return input;
    }    

    function removeScheduleController($scope, $modalInstance, schedule) {
        $scope.schedule = schedule;
        $scope.ok = function () {
            $modalInstance.close();
        };
        
        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };            
    }    

    //schedulesController
    function schedulesController($scope, $route, $rootScope, $location, $modal, StackService){
        
        $scope.userId = StackService.currentUser().id;

        StackService.getUserSchedules($scope.userId).success(function(response) {
            $scope.schedules = response;
        }).error(function (data, status) {
                //notifier.onError(data.message);
        });

        $scope.remove = function(schedule) {
            var modalInstance = $modal.open({
                templateUrl: 'app/views/schedules/removeScheduleModal',
                controller: removeScheduleController,
                resolve: {
                    schedule: function () {
                        return angular.copy(schedule);
                    }
                }
            });
            
            modalInstance.result.then(function() {
                StackService.deleteSchedule(schedule.id).success(function(response) {
                    $route.reload();
                }).error(function (data, status) {
                    //notifier.onError(data.message);
                });
            });
        };

    }
    
     // Create Schedules
    function createSchedulesController($scope, $rootScope, $location, StackService ){
        $scope.showDayContainer = false;
        $scope.showDatesContainer = false;
        $scope.schedule = {};
        $scope.range = range; 

        $scope.recurrenceOptions = [{name:"Daily", value:'daily'}, {name:"Weekly", value:'weekly'}, {name:"Monthly", value:'monthly'}];
        $scope.hourOptions = rangeObj(0,24);
        $scope.minuteOptions = rangeObj(0, 50, 10);
    
        $scope.schedule.recurrence = $scope.recurrenceOptions[0];
        $scope.schedule.dayHour = $scope.hourOptions[10];
        $scope.schedule.dayMinute = $scope.minuteOptions[1];
        $scope.days = [];
        $scope.weekday = [];
        
        $scope.selectRecurrenceType = function(){
            $scope.showDayContainer = false;
            $scope.showDatesContainer = false;
            if ($scope.schedule.recurrence == "weekly" || $scope.schedule.recurrence.value == "weekly") {
                $scope.showDayContainer = true;
            } 
            
            if ($scope.schedule.recurrence  == "monthly" || $scope.schedule.recurrence.value  == "monthly") {
                 $scope.showDatesContainer = true;
            }  
        }

        $scope.userId = StackService.currentUser().id;
        
        $scope.selectWeekDay = function(weekDay){
            var index = $scope.weekday.indexOf(weekDay);
            if (index >= 0) {
               $scope.weekday.splice(index, 1);
               $( "#" + weekDay).removeClass('active');
            } else{
                $scope.weekday.push(weekDay);
                $( "#" + weekDay).addClass('active');	
            }                
        }

        $scope.selectDate = function(day){
            var index = $scope.days.indexOf(day);
            if (index >= 0) {
               $scope.days.splice(index, 1);
               this.isActive = false;
            } else{
                $scope.days.push(day);
                this.isActive = true;	
            }
        }
        $scope.scheduleSubmit = function(schedule){
            var scheduleObj = {
                userId: $scope.userId,
                name: schedule.name,
                recurrence: schedule.recurrence.value,
                hour: schedule.dayHour.value,
                minute: schedule.dayMinute.value,
            }

            if(schedule.recurrence.value == 'weekly'){
                scheduleObj.week =  $scope.weekday
            }

            if(schedule.recurrence.value == 'monthly'){
                scheduleObj.day =  $scope.days;
                
            }

            StackService.createSchedule(scheduleObj).success(function(response) {
                $location.path('/schedules');
            }).error(function (data, status) {
                //notifier.onError(data.message);
            });
        }
    }
   
    // Edit Schedules
    function editSchedulesController($scope, $rootScope, $routeParams, $location, $timeout, StackService ){
        $scope.params = $routeParams;
        $scope.range = range;
        $scope.country = 'US';
        $scope.recurrenceOptions = [{name:"Daily", value:'daily'}, {name:"Weekly", value:'weekly'}, {name:"Monthly", value:'monthly'}];
        $scope.hourOptions = rangeObj(0,24);
        $scope.minuteOptions = rangeObj(0, 50, 10);
        $scope.selectVal =  function(){
            $scope.days = [];            
            $scope.weekdays = [];
        
           var days = $scope.schedule.day;
            var weekDays = $scope.schedule.week;
            var dayHr = $scope.schedule.hour;
            var dayMin = $scope.schedule.minute;

            if($scope.schedule.recurrence){
                angular.forEach($scope.recurrenceOptions, function(obj, index){
                    if(obj.value == $scope.schedule.recurrence){
                        $scope.schedule.recurrence = $scope.recurrenceOptions[index]  
                    }    
                });
            }

            if(dayMin) {
                angular.forEach($scope.minuteOptions, function(obj, index){
                    if(obj.value == dayMin){
                        $scope.schedule.dayMinute = $scope.minuteOptions[index]  
                    }    
                });
            }
            
            if(dayHr){
                angular.forEach($scope.hourOptions, function(obj, index){
                    if(obj.value == dayHr){
                        $scope.schedule.dayHour = $scope.hourOptions[index]  
                    }    
                });
            }
            
            if(days){
                var splitDays = days.split(',');
                angular.forEach(splitDays, function(day){
                    if(day != ','){
                        $scope.days.push(parseInt(day));
                        $( "#" + day).addClass('active')
                    } 
                });
            }
            
            if(weekDays){
                var splitWeekDays  = weekDays.split(',');  
                angular.forEach(splitWeekDays, function(day){
                    if(day != ','){
                        $scope.weekdays.push(day);
                        $( "#" + day).addClass('active')
                    } 
                });               
            }            
        }
                    
        $scope.selectRecurrenceType = function(){
            $scope.showDayContainer = false;
            $scope.showDatesContainer = false;
            
            if ($scope.schedule.recurrence == "weekly" || $scope.schedule.recurrence.value == "weekly") {
                $scope.showDayContainer = true;
            } 
            
            if ($scope.schedule.recurrence  == "monthly" || $scope.schedule.recurrence.value  == "monthly") {
                 $scope.showDatesContainer = true;
            } 
            $timeout(function(){
                $scope.selectVal();
            }, 200);        
           
        }

        $scope.selectWeekDay = function(weekDay){
            var index = $scope.weekdays.indexOf(weekDay);
            if (index >= 0) {
               $scope.weekdays.splice(index, 1);
               $( "#" + weekDay).removeClass('active');
            } else{
                $scope.weekdays.push(weekDay);
                $( "#" + weekDay).addClass('active');	
            }                
        }

        $scope.selectDate = function(day){
            var index = $scope.days.indexOf(day);
            if (index >= 0) {
               $scope.days.splice(index, 1);
               this.isActive = false;
            } else{
                $scope.days.push(day);
                 this.isActive = true;	
            }
        }
        
        $scope.loadSchedule = function() { 
            var id = $scope.params["id"];
            StackService.getSchedule(id).success(function(response) {
                var data = response;
                var viewObj = {};
                if(data.week){
                    viewObj.week = data.week;
                }

                if(data.day){
                    viewObj.day = data.day;
                }
                
                if(data.hour){
                    viewObj.hour = data.hour;    
                }
                
                if(data.minute){
                    viewObj.minute = data.minute;    
                }
                
                data = angular.extend(viewObj, data);
                $scope.schedule = angular.copy(data);
            }).error(function (data, status) {
                    //notifier.onError(data.message);
            });
        };
        
        $timeout($scope.selectRecurrenceType, 100);
        $scope.loadSchedule();
        $timeout(function(){
            $scope.selectVal();
        }, 200);        
        
        $scope.update = function(schedule){
            var scheduleId = schedule.id;
            var scheduleObj = {
                name: schedule.name,
                recurrence: schedule.recurrence.value,
                hour: schedule.dayHour.value,
                minute: schedule.dayMinute.value,
            }

            if(schedule.recurrence.value == 'weekly'){
                scheduleObj.week =  $scope.weekdays;
            }

            if(schedule.recurrence.value == 'monthly'){
                scheduleObj.day =  $scope.days;
            }   

            StackService.updateSchedule(scheduleId, scheduleObj).success(function(response) {
                $location.path('/schedules');
            }).error(function (data, status) {
                    //notifier.onError(data.message);
            });
         }        

    }

    function config($routeProvider){
        $routeProvider
        .when("/schedules", {
            templateUrl: "app/views/schedules/index.html",
            controller: "schedulesController",
            resolve: {
                loginRequired: function (AuthService) {
                    return AuthService.loginRequired();
                }
            }           
        })
        .when("/schedules/create", {
            templateUrl: "app/views/schedules/new.html",
            controller: "createSchedulesController",
            resolve: {
                loginRequired: function (AuthService) {
                    return AuthService.loginRequired();
                }
            }             
        })
        .when("/schedules/edit/:id", {
            templateUrl: "app/views/schedules/edit.html",
            controller: "editSchedulesController",
            resolve: {
                loginRequired: function (AuthService) {
                    return AuthService.loginRequired();
                }
            }             
        })

    }

    angular.module("stack.schedules", ["ngRoute"])
        .config(["$routeProvider", config])
        .controller("schedulesController", schedulesController)
        .controller("createSchedulesController", createSchedulesController)
        .controller("editSchedulesController", editSchedulesController);


})();

