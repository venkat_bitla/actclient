/* global angular */
"use strict";
(function(){
    
    var OptActions = [{name:"Start EC2 Instance", value:'start'}, {name:"Stop EC2 Instance", value:'stop'}];

       
    //Add Action
    function addActionController($scope, $modalInstance, StackService, Notifier) {
        
        var notifier = Notifier.build('Add Action');
        $scope.showNotifications = function(){
            notifier.showNotifications();  
        };
        
        $scope.action = {};
        $scope.actionOptions = OptActions;
        $scope.action.actionType = 'start';
        $scope.userId = StackService.currentUser().id;
        $scope.schedules = [];
        $scope.instances = [];
        
        StackService.getUserSchedules($scope.userId).success(function(response) {
            $scope.schedules = response;
        }).error(function (data, status) {
            notifier.onError(data.message);
        });      

        StackService.getUserInstances($scope.userId).success(function(response) {
            $scope.instances = response;
        }).error(function (data, status) {
            notifier.onError(data.message || '');
        });               
               
        $scope.addAaction = function () {
            if($scope.action_form.$valid) {
                var actionObj = {
                    userId: $scope.userId, 
                    name: $scope.action.name,
                    instanceId: 1, //$scope.action.instance.id,
                    scheduleId: $scope.action.schedule.id,
                    action: $scope.action.actionType.value
                }

                StackService.createAction(actionObj).success(function (response) {
                    $modalInstance.close(response);
                }).error(function (data, status) {
                
                });
            }
        };
               
        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };            
    }
    
    // Edit Action
    function editActionController($scope, $modalInstance, $timeout, StackService, Notifier, action) {
 
        var notifier = Notifier.build('Add Action');
        $scope.showNotifications = function(){
            notifier.showNotifications();  
        };

        $scope.action = action;
        $scope.actionOptions = OptActions;
        $scope.userId = StackService.currentUser().id;
        $scope.schedules = [];
        $scope.instances = [];
        
        $scope.action.instance = $scope.action.instance_id;
        $scope.action.schedule = $scope.action.schedule_id;

        StackService.getUserSchedules($scope.userId).success(function(response) {
            $scope.schedules = response;
        }).error(function (data, status) {
            notifier.onError(data.message);
        });      

        StackService.getUserInstances($scope.userId).success(function(response) {
            $scope.instances = response;
        }).error(function (data, status) {
            notifier.onError(data.message || '');
        });               

        //set default options
        $timeout(function(){
           angular.forEach(OptActions, function(obj, index){
             if(obj.value == $scope.action.action){
                 $scope.action.actionType = OptActions[index]  
             }
           });
           $('#actionInstance').val($scope.action.instance_id);
           $('#actionSchedule').val($scope.action.schedule_id);
        }, 200);   
         
        $scope.eidt = function () {
            if($scope.action_form.$valid) {
                var id = $scope.action.id;
                var actionObj = {
                    name: $scope.action.name,
                    instanceId: $scope.action.instance,
                    scheduleId: $scope.action.schedule,
                    action: $scope.action.actionType.value
                } 
                
                StackService.updateAction(id, actionObj).success(function(response) {
                    $modalInstance.close(response);
                }).error(function (data, status) {
                });
            }
        };
        
        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };            
    }   
    
    function removeActionController($scope, $modalInstance, action) {
        $scope.action = action;
        $scope.ok = function () {
            $modalInstance.close();
        };
        
        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };            
    }    
    
    //actions Controller
    function actionsController($scope, $route, $rootScope, $location, $modal, StackService, Notifier){
        var notifier = Notifier.build('Action');
        $scope.showNotifications = function(){
            notifier.showNotifications();  
        };
        
        $scope.userId = StackService.currentUser().id;
        StackService.getUserActions($scope.userId).success(function (response) {
            $scope.actions = response;
        }).error(function (data, status) {
            notifier.onError(data.message);
        });
        
        // Add Action
        $scope.add = function() {
            var modalInstance = $modal.open({
                templateUrl: 'app/views/actions/addActionModal.html',
                controller: addActionController,
            });
            
            modalInstance.result.then(function(response) {
                $route.reload();
            });
        };         

        //Edit Action
        $scope.edit = function(action) {
           $scope.action = {};
            var modalInstance = $modal.open({
                templateUrl: 'app/views/actions/editActionModal.html',
                controller: editActionController,
                 resolve: {
                    action: function () {
                        return angular.copy(action);
                    }
                }
            });
            
            // modalInstance.opened.then(function() {
            // });
             
            modalInstance.result.then(function() {
                $route.reload();
            });
        };          
        
         //Remove Action
         $scope.remove = function(action) {
            var modalInstance = $modal.open({
                templateUrl: 'app/views/actions/removeActionModal.html',
                controller: removeActionController,
                resolve: {
                    action: function () {
                        return angular.copy(action);
                    }
                }
            });
            
            modalInstance.result.then(function() {
                StackService.deleteAction(action.id).success(function () {
                    $route.reload();
                }).error(function (data, status) {
                    notifier.onError(data.message);
                });
            });
        };       
               
        
    }

    
    function config($routeProvider){
        $routeProvider
        .when("/actions", {
            templateUrl: "app/views/actions/index.html",
            controller: "actionsController",
            resolve: {
                loginRequired: function (AuthService) {
                    return AuthService.loginRequired();
                }
            }           
        })
    }
    
    angular.module("stack.actions", ["ngRoute"])
        .config(["$routeProvider", config])
        .controller("actionsController", actionsController)
})();
