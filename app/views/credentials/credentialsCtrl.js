/* global angular */
"use strict";
(function(){
    
    var typeOptions = [{name:"AWS", value:'aws'}, {name:"google", value:'google'}];
    var awsRegions = [
                {name:"US East (N. Virginia)", value:'us-east-1'}, 
                {name:"US West (Oregon)", value:'us-west-2'}, 
                {name:"US West (N. California)", value:'us-west-1'},
                {name:"EU (Ireland)", value:'eu-west-1'},
                {name:"EU (Frankfurt)", value:'eu-central-1'},
                {name:"Asia Pacific (Singapore)", value:'ap-southeast-1'},
                {name:"Asia Pacific (Sydney)", value:'Asia Pacific (Sydney)'},
                {name:"Asia Pacific (Tokyo)", value:'ap-northeast-1'},
                {name:"South America (Sao Paulo)", value:'sa-east-1'},
    ];

    function removeCredentialController($scope, $modalInstance, credential) {
        $scope.credential = credential;
        $scope.ok = function () {
            $modalInstance.close();
        };
        
        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };            
    }    
    
    //credentialsController
    function credentialsController($scope, $route, $rootScope, $location, $modal, StackService, Notifier){
        
        var notifier = Notifier.build('Credentials');
        $scope.showNotifications = function(){
            notifier.showNotifications();  
        };

        $scope.credentials = [];
        $scope.userId = StackService.currentUser().id;

        StackService.getUserCredentials($scope.userId).success(function (response) {
            $scope.credentials = response;
        }).error(function (data, status) {
            notifier.onError(data.message);
        });

        $scope.remove = function(credential) {
            var modalInstance = $modal.open({
                templateUrl: 'app/views/credentials/removeCredentialModal.html',
                controller: removeCredentialController,
                resolve: {
                    credential: function () {
                            return angular.copy(credential);
                    }
                }
            });
            
            modalInstance.result.then(function() {
                StackService.deleteCredential(credential.id).success(function () {
                    notifier.onSuccess('Credentials deleted sucesssfully');
                    $route.reload();
                }).error(function (data, status) {
            		notifier.onError(data.message);
                });
            });
        };

    }

    function createCredentialsController($scope, $rootScope, $location, StackService, Notifier){

        var notifier = Notifier.build('Create Credentials');
        $scope.showNotifications = function(){
            notifier.showNotifications();  
        };
            
        $scope.credential = {};
        $scope.typeOptions = typeOptions;
        $scope.credential.type = 'aws';
        $scope.regionOptions = awsRegions;
        $scope.credential.region = 'us-east-1';
        
        $scope.userId = StackService.currentUser().id;

         $scope.create = function(credential){
            var credentialArg = angular.extend({userId: $scope.userId}, credential);
            StackService.createCredential(credentialArg).success(function(response) {
                notifier.onSuccess('Credentials created sucesssfully');
                $location.path('/credentials');
            }).error(function (data, status) {
                notifier.onError(data.message);
            });
         }
        
    }

    function editCredentialsController($scope, $rootScope, $routeParams, $location, $timeout, StackService, Notifier) {

        var notifier = Notifier.build('Edit Credentials');
        $scope.showNotifications = function(){
            notifier.showNotifications();  
        };
        
        $scope.credential = {};
        $scope.typeOptions = typeOptions;
        $scope.params = $routeParams;
         $scope.regionOptions = awsRegions;
        $scope.credential.region = 'us-east-1';       

        $scope.loadCredential = function() { 
            var id = $scope.params["id"];
            StackService.getCredential(id).success(function (response) {
                $scope.credential = response;
            }).error(function (data, status) {
                notifier.onError(data.message);
            });
            
        };
 
       $timeout($scope.selChange, 100);

        $scope.loadCredential();
        $scope.userId = StackService.currentUser().id;

        $scope.update = function(credential){
            StackService.updateCredential(credential).success(function (response) {
                notifier.onSuccess('Credentials uptaded sucesssfully');
                $location.path('/credentials');
            }).error(function (data, status) {
                notifier.onError(data.message);
            });
         }
    }

    function config($routeProvider){
        $routeProvider
        .when("/credentials", {
            templateUrl: "app/views/credentials/index.html",
            controller: "credentialsController",
            resolve: {
                loginRequired: function (AuthService) {
                    return AuthService.loginRequired();
                }
            }
        })
        .when("/credentials/create", {
            templateUrl: "app/views/credentials/new.html",
            controller: "createCredentialsController",
            resolve: {
                loginRequired: function (AuthService) {
                    return AuthService.loginRequired();
                }
            }        
        })
        .when("/credentials/edit/:id", {
            templateUrl: "app/views/credentials/edit.html",
            controller: "editCredentialsController",
            resolve: {
                loginRequired: function (AuthService) {
                    return AuthService.loginRequired();
                }
            }            
        })

    }

    angular.module("stack.credentials", ["ngRoute"])
        .config(["$routeProvider", config])
        .controller("credentialsController", credentialsController)
        .controller("createCredentialsController", createCredentialsController)
        .controller("editCredentialsController", editCredentialsController);

})();

