"use strict";
(function(){
    //headerController
    function headerController($scope, $rootScope, $window, $location, StackService, stackSocket){
	   $scope.showHeader = false;
       $scope.user = '';
       $scope.logout = function() {
          $window.sessionStorage.removeItem('user');
          $window.sessionStorage.setItem("isLogout", true);
          $scope.showHeader = false;
          $location.path('/login');
          stackSocket.emit('disconnect');
       };
       
       $rootScope.$on('stack.routeChangeEvent', function(event, argsObj) {
            $scope.showHeader = false;
            var userLogedIn = StackService.currentUser(); 
            if(userLogedIn && $location.path() !== '/login'){
               $scope.showHeader = true;
               $scope.user = userLogedIn;
            }
       });
       
    }
	
    headerController.$inject =  ["$scope", "$rootScope", "$window", "$location", "StackService", "stackSocket"];

    angular.module("stack.header", ["ngRoute"])
    .controller("headerController", headerController);


})();

